import os, sys, glob, re, shutil
from utilities import *
from LatexUtilities import *
from HtmlUtilities import *

mainTexFilePath = sys.argv[1]
suppIncludeTexFilePath = sys.argv[2]
suppTexFilePath = sys.argv[3]
mainHtmlFilePath = sys.argv[4]
suppIncludeHtmlFilePath = sys.argv[5]
outputDirPath = sys.argv[6]

def process(tex, html, tagPrefix, labelCountPrefix):
    figureCount = 0
    figureLabelOrderDict = {}

    if tex == None:
        return tex, html, figureLabelOrderDict

    for figureMatch in re.finditer("\[%s:.*\]" % tagPrefix, tex):
        figureCount += 1

        token = figureMatch.group(0)
        tokenItems = token.replace("[%s:" % tagPrefix, "").replace("]", "").split("|")
        label = tokenItems[0]
        width = tokenItems[1]
        orientation = tokenItems[2]
        title = tokenItems[3]
        description = tokenItems[4]

        figureLabelOrderDict[label] = figureCount

        expanded = ""

        if orientation == "landscape":
            expanded += "\\begin{landscape}\n"

        expanded += "\\begin{figure}[!ht]\n"
        #expanded += "\caption{\\doublespacing \\textbf{%s.} %s}\n" % (title, description)

        extension = "pdf"
        if not os.path.exists("%s.%s" % (label, extension)):
            extension = "png"
            if not os.path.exists("%s.%s" % (label, extension)):
                extension = None

        if extension == None:
            print "Missing figure file for %s" % label
            exit(1)
            #expanded += "\\textbf{Missing figure file for %s}\n" % label.replace("_", "\_")
        else:
            #expanded += "\centerline{\n\includegraphics[width=%s in, type=%s, ext=.%s, read=.%s]{%s}\n}\n" % (width, extension, extension, extension, label)
            expanded += "\includegraphics[width=%s in, type=%s, ext=.%s, read=.%s]{%s}\n" % (width, extension, extension, extension, label)

            sourceFilePath = "%s.%s" % (label, extension)
            destFilePath = "%s/Figure_%s%i.%s" % (outputDirPath, labelCountPrefix, figureCount, extension)
            print "Copying %s to %s" % (sourceFilePath, destFilePath)
            shutil.copy(sourceFilePath, destFilePath)

        expanded += "\label{%s}\n" % label
        expanded += "\end{figure}\n"

        if orientation == "landscape":
            expanded += "\\end{landscape}\n"

        expanded += "\\doublespacing \\noindent \\textbf{Figure %s%i: %s}. %s\n" % (labelCountPrefix, figureCount, title, description)

        expanded += "\\clearpage\n"

        tex = tex.replace(token, expanded)

        if html != None:
            figureHtml = buildFigureHtml("Figure %s%i: %s" % (labelCountPrefix, figureCount, title), description)
            html = html.replace(token, figureHtml)

    return tex, html, figureLabelOrderDict

mainTex = readTextFromFile(mainTexFilePath)
mainHtml = readTextFromFile(mainHtmlFilePath)

suppIncludeTex = None
suppTex = None
if os.path.exists(suppTexFilePath):
    suppIncludeTex = readTextFromFile(suppIncludeTexFilePath)
    suppTex = readTextFromFile(suppTexFilePath)

suppIncludeHtml = None
if os.path.exists(suppIncludeHtmlFilePath):
    suppIncludeHtml = readTextFromFile(suppIncludeHtmlFilePath)

mainTex, mainHtml, mainFigureLabelOrderDict = process(mainTex, mainHtml, "Figure", "")
suppIncludeTex, suppIncludeHtml, suppFigureLabelOrderDict = process(suppIncludeTex, suppIncludeHtml, "SupplementaryFigure", "S")
suppTex, ignore1, ignore2 = process(suppTex, None, "SupplementaryFigure", "S")

mainTex = convertRefsToNumbers(mainTex, mainFigureLabelOrderDict, suppFigureLabelOrderDict)
mainHtml = convertRefsToNumbers(mainHtml, mainFigureLabelOrderDict, suppFigureLabelOrderDict)

writeScalarToFile(mainTex, mainTexFilePath)
writeScalarToFile(mainHtml, mainHtmlFilePath)

if suppIncludeTex != None:
    suppIncludeTex = convertRefsToNumbers(suppIncludeTex, mainFigureLabelOrderDict, suppFigureLabelOrderDict)
    suppIncludeHtml = convertRefsToNumbers(suppIncludeHtml, mainFigureLabelOrderDict, suppFigureLabelOrderDict)
    suppTex = convertRefsToNumbers(suppTex, mainFigureLabelOrderDict, suppFigureLabelOrderDict)
    suppTex = suppTex.replace("[TableOfContentsFigures]", addTableOfContentsItems("Figure", suppFigureLabelOrderDict, labelPrefix="S"))

    writeScalarToFile(suppIncludeTex, suppIncludeTexFilePath)
    writeScalarToFile(suppIncludeHtml, suppIncludeHtmlFilePath)
    writeScalarToFile(suppTex, suppTexFilePath)
