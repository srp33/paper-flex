import re, sys

medlineFile = open("J_Medline.txt", 'r')
medlineFile.readline()

medlineFileText = ""
for line in medlineFile:
    medlineFileText += line

medlineFile.close()

medlineEntries = medlineFileText.split("--------------------------------------------------------")
medlineEntryParts = [entry.split("\n") for entry in medlineEntries]

def getJournalAbbreviation(journal):
    for parts in medlineEntryParts:
        title = parts[2].replace("JournalTitle: ", "")
        abbr = parts[3].replace("MedAbbr: ", "")

        if journal.strip().lower() == title.strip().lower():
            return abbr
    return journal

def formatTitle(title, style):
    words = title.rstrip().replace("{", "").replace("}", "").rstrip(".").split(" ")
    modWords = []
    for i in range(len(words)):
        word = words[i]

        if keepTitleWordAsIs(word):
            modWords.append(word)
        elif i == 0 or words[i-1].endswith(":"):
            modWords.append(word.capitalize())
        else:
            modWords.append(word.lower())

    return "{" + " ".join(modWords) + "}"

def parseAuthors(author, style):
    authorNameSeparator = style.getConfigValue("author.name.separator")

    modAuthorList = []
    for anAuthor in author.split(" and "):
        authorParts = anAuthor.split(", ")
        lastName = authorParts[0]

        if len(authorParts) > 1:
            if authorParts[1].replace(".", "").replace(" ", "").isupper():
                initials = [x for x in authorParts[1].replace(".", "").replace(" ", "")]
            else:
                otherNames = re.split("\s", authorParts[1])
                initials = [x[0].upper() for x in otherNames if len(x) > 0]

            if style.getConfigValue("use.author.initial.periods") == "True":
                initials = [x + "." for x in initials]

            if style.getConfigValue("put.author.last.name.last") == "True":
                modAuthor = "".join(initials) + " " + lastName
            else:
                modAuthor = lastName + authorNameSeparator + " " + "".join(initials)
        else:
            modAuthor = lastName

        modAuthorList.append(modAuthor)

    return modAuthorList

def formatAuthor(author, style):
    maxNumAuthors = int(style.getConfigValue("max.num.authors"))
    numAuthorsWhenAboveMax = int(style.getConfigValue("num.authors.when.above.max"))
    addAnd = style.getConfigValue("and.before.last.author") == "True"

    modAuthors = parseAuthors(author, style)

    if maxNumAuthors > 0 and len(modAuthors) > maxNumAuthors:
        modAuthors = modAuthors[:maxNumAuthors]

        if numAuthorsWhenAboveMax < maxNumAuthors:
            modAuthors = modAuthors[:numAuthorsWhenAboveMax]

        modAuthors.append("et al")

    if len(modAuthors) == 1:
        return modAuthors[0]

    if addAnd:
        if len(modAuthors) == 2:
            if modAuthors[-1] != "et al":
                return " and ".join(modAuthors)

        if len(modAuthors) > 2:
            modAuthors[-1] = "and " + modAuthors[-1]

    return ", ".join(modAuthors).rstrip()

def formatEditor(editor, style):
    modEditors = parseAuthors(editor, style)
    modEditor = ", ".join(modEditors).rstrip()

    if len(modEditors) == 1:
        return modEditor + ", ed"
    return modEditor + ", eds"

def keepTitleWordAsIs(word):
    if word in properNouns:
        return True
    if word.replace("-", "").isupper():
        return True
    if len(re.findall("[A-Z]", word)) - len(re.findall("-", word)) > 1:
        return True

    return False

def parseEntryDict(entry):
    entryLines = entry.rstrip().split("\n")
    firstLine = entryLines.pop(0)
    entryType = firstLine.split("{")[0]
    entryKey = firstLine.split("{")[1].replace(",", "")

    entryDict = {}

    for line in entryLines:
        if line.find(" = ") == -1:
            continue

        key = line.split(" = ")[0]
        value = line.split(" = ")[1]

        if value.endswith(","):
            value = value[:-1]
        value = value.lstrip().rstrip()[1:-1] # Remove leading and trailing squiggly bracket

        entryDict[key] = value

    return entryKey, entryType, entryDict

def processBibFiles(bibTexFilePaths, style, bblEntryKeysLong, bblEntryKeysShort):
    allEntryDict = {}

    for bibTexFilePath in bibTexFilePaths:
        text = "".join([line for line in file(bibTexFilePath)])

        for entry in text.split("\n@"):
            if entry.split("\n")[0].find("{") == -1: #These if statements accommodate header lines in .bib files
                continue
            if entry.startswith("@"):
                entry = entry[1:]

            entryKey, entryType, entryDict = parseEntryDict(entry)
            allEntryDict[entryKey] = (entryType, entryDict)

    output = ""

    for i in range(len(bblEntryKeysShort)):
        entryKeyShort = bblEntryKeysShort[i]
        entryKeyLong = bblEntryKeysLong[i]
        entryType = allEntryDict[entryKeyShort][0]
        entryDict = allEntryDict[entryKeyShort][1]

        reformat = True
        if entryType.find("_noreformat") > -1:
            entryType = entryType.replace("_noreformat", "")
            reformat = False

        if reformat:
            if entryDict.has_key("journal"):
                entryDict["journal"] = getJournalAbbreviation(entryDict["journal"]).rstrip()
            if entryDict.has_key("title"):
                entryDict["title"] = formatTitle(entryDict["title"], style)
            if entryDict.has_key("author"):
                entryDict["author"] = formatAuthor(entryDict["author"], style)
            if entryDict.has_key("editor"):
                entryDict["editor"] = formatEditor(entryDict["editor"], style)

        output += "\\" + bblKey + entryKeyLong + "\n"

        templateText = style.getTemplate(entryType)
        for key in entryDict:
            templateText = templateText.replace("#" + key + "#", entryDict[key])

        for match in re.finditer("<[\#\w\d\s\.\-,;:\(\)]+>", templateText):
            token = match.group(0)
            if token.find("#") > -1:
                templateText = templateText.replace(token, "")

        templateText = templateText.replace("<", "").replace(">", "")
        templateText = templateText.replace("?.", "?") #This is a workaround for now. If a reference ends with a question mark, we don't want to also have a period after it. However, this should be generalized to other sentence-ending punctuation marks.

        output += templateText + "\n\n"

    return output

class Style:
    def __init__(self, filePath):
        configFileLines = [line for line in file(filePath) if not line.startswith("@")]
        templateFileLines = [line for line in file(filePath) if line.startswith("@")]

        self.configDict = {}
        for line in configFileLines:
            lineItems = line.rstrip("\n").split("=")
            self.configDict[lineItems[0]] = lineItems[1]

        self.templateDict = {}
        for line in templateFileLines:
            lineItems = line.rstrip().split("=")
            self.templateDict[lineItems[0].replace("@", "")] = lineItems[1]

    def getConfigValue(self, key):
        return self.configDict[key]

    def getTemplate(self, key):
        return self.templateDict[key]

bblFilePath = sys.argv[1]
bblKey = sys.argv[2]
bibTexFilePaths = sys.argv[3].split(",")
style = Style(sys.argv[4])
properNounsFilePath = sys.argv[5]

bblFile = open(bblFilePath)
bblFileLines = [line for line in bblFile if not line.startswith("#")]
bblFile.close()

properNouns = []
if properNounsFilePath != "None":
    properNouns = [x.rstrip() for x in file(properNounsFilePath)]

bblHeader = bblFileLines.pop(0)
bblFooter = bblFileLines.pop(-1)

bblText = ""
for line in bblFileLines:
    bblText += line

bblRawEntries = bblText.split("\\" + bblKey)

bblEntryKeysLong = []
bblEntryKeysShort = []

for rawEntry in bblRawEntries:
    if len(rawEntry.strip()) == 0:
        continue

    rawEntryItems = rawEntry.split("\n")
    entry = rawEntryItems.pop(0)
    while not entry.endswith("}"):
        entry += rawEntryItems.pop(0)

    bblEntryKeysLong.append(entry)
    entry = entry[entry.rfind("{")+1:-1]
    bblEntryKeysShort.append(entry)

bblFile = open(bblFilePath, 'w')
bblFile.write(bblHeader + "\n\n")
bblFile.write(processBibFiles(bibTexFilePaths, style, bblEntryKeysLong, bblEntryKeysShort))
bblFile.write(bblFooter)
bblFile.close()
