import os, sys, glob, re
from utilities import *
from HtmlUtilities import *

texFilePath = sys.argv[1]
htmlFilePath = sys.argv[2]

tex = readTextFromFile(texFilePath)

html = None
if os.path.exists(htmlFilePath):
    html = readTextFromFile(htmlFilePath)

equationCount = 0

for match in re.finditer("\[Equation:.*\]", tex):
    equationCount += 1

    token = match.group(0)
    tokenItems = token.replace("[Equation:", "").replace("]", "").split("|")
    label = tokenItems[0]
    height = tokenItems[1]
    equation = tokenItems[2]

    expanded = "\\begin{equation}\\label{%s}\n%s\n\\end{equation}\n" % (label, equation)
    tex = tex.replace(token, expanded)

    if html != None:
        writeScalarToFile(equation, "Equations/Equation_%i.tex" % equationCount)
        pngFilePath = "Equation_%i.png" % equationCount
        html = html.replace(token, buildEquationHtml(pngFilePath, height))

writeScalarToFile(tex, texFilePath)

if html != None:
    writeScalarToFile(html, htmlFilePath)
