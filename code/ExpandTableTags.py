import os, sys, glob, re, shutil
import utilities
from LatexUtilities import *
from HtmlUtilities import *

mainTexFilePath = sys.argv[1]
suppIncludeTexFilePath = sys.argv[2]
suppTexFilePath = sys.argv[3]
mainHtmlFilePath = sys.argv[4]
suppIncludeHtmlFilePath = sys.argv[5]
outputDirPath = sys.argv[6]

def process(tex, html, tagPrefix, labelCountPrefix):
    tableCount = 0
    tableLabelOrderDict = {}

    if tex == None:
        return tex, html, tableLabelOrderDict

    for match in re.finditer("\[%s:.*\]" % tagPrefix, tex):
        tableCount += 1

        tag = match.group(0)
        tagItems = tag.replace("[%s:" % tagPrefix, "").replace("]", "").split("|")
        #tagItems = line.strip().replace("[", "").replace("]", "").replace("%s:" % tagPrefix, "").split("|")
        label = tagItems[0]
        width = tagItems[1]
        orientation = tagItems[2]
        title = tagItems[3]
        description = tagItems[4]

        tableLabelOrderDict[label] = tableCount

        tableTex = "{\n\\setlength{\\parindent}{0cm}\n"
        tableTex += "\\begin{minipage}[t]{1.0\\textwidth}\n"
        tableTex += "\\doublespacing \\textbf{Table %s%i: %s}. %s\n\n" % (labelCountPrefix, tableCount, title, description)
        tableTex += "\\vspace{7 mm}\n"
        tableTex += "\\label{%s}\n" % label
        if width == "":
            tableTex += "\\includegraphics[type=pdf, ext=.pdf, read=.pdf]{%s}\n" % label
        else:
            tableTex += "\\includegraphics[width=%s in, type=pdf, ext=.pdf, read=.pdf]{%s}\n" % (width, label)
        tableTex += "\\end{minipage}\n"
        tableTex += "}\n"
        tableTex += "\\clearpage\n"
        shutil.copy("%s.pdf" % label, "%s/Table_%s%i.pdf" % (outputDirPath, labelCountPrefix, tableCount))

        tex = tex.replace(tag, tableTex)

        if html != None:
            # When we do this, we should move it into ConvertTableTextToPdf script
            #os.system("convert -density 300 -depth 8 -quality 100 -resize 32%% %s.pdf %s.png" % (label, label))
            #html = html.replace(tag, "Table %i: %s. %s\n\n<img src='%s' />" % (tableCount, title, description, label + ".png"))
            html = html.replace(tag, "<b>Table %s%i: %s</b>. %s" % (labelCountPrefix, tableCount, title, description))

    return tex, html, tableLabelOrderDict

mainTex = readTextFromFile(mainTexFilePath)
mainHtml = readTextFromFile(mainHtmlFilePath)

suppIncludeTex = None
suppTex = None
if os.path.exists(suppTexFilePath):
    suppIncludeTex = readTextFromFile(suppIncludeTexFilePath)
    suppTex = readTextFromFile(suppTexFilePath)

suppIncludeHtml = None
if os.path.exists(suppIncludeHtmlFilePath):
    suppIncludeHtml = readTextFromFile(suppIncludeHtmlFilePath)

mainTex, mainHtml, mainTableLabelOrderDict = process(mainTex, mainHtml, "Table", "")
suppIncludeTex, suppIncludeHtml, suppTableLabelOrderDict = process(suppIncludeTex, suppIncludeHtml, "SupplementaryTable", "S")
suppTex, ignore1, ignore2 = process(suppTex, suppIncludeHtml, "SupplementaryTable", "S")

mainTex = convertRefsToNumbers(mainTex, mainTableLabelOrderDict, suppTableLabelOrderDict)
mainHtml = convertRefsToNumbers(mainHtml, mainTableLabelOrderDict, suppTableLabelOrderDict)

utilities.writeScalarToFile(mainTex, mainTexFilePath)
utilities.writeScalarToFile(mainHtml, mainHtmlFilePath)

if suppTex != None:
    suppIncludeTex = convertRefsToNumbers(suppIncludeTex, mainTableLabelOrderDict, suppTableLabelOrderDict)
    suppIncludeHtml = convertRefsToNumbers(suppIncludeHtml, mainTableLabelOrderDict, suppTableLabelOrderDict)
    suppTex = convertRefsToNumbers(suppTex, mainTableLabelOrderDict, suppTableLabelOrderDict)
    suppTex = suppTex.replace("[TableOfContentsTables]", addTableOfContentsItems("Table", suppTableLabelOrderDict, labelPrefix="S"))

    utilities.writeScalarToFile(suppIncludeTex, suppIncludeTexFilePath)
    utilities.writeScalarToFile(suppIncludeHtml, suppIncludeHtmlFilePath)
    utilities.writeScalarToFile(suppTex, suppTexFilePath)
