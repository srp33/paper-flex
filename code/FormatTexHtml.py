import os, sys, glob, re
from utilities import *
from ReferenceUtilities import *

inFilePath = sys.argv[1]
bibFilePath = sys.argv[2]
bibStyle = Style(sys.argv[3])
medlineFilePath = sys.argv[4]
properNounsFilePath = sys.argv[5]
titleFilePath = sys.argv[6]
authorsFilePath = sys.argv[7]
institutionsFilePath = sys.argv[8]
titleFootnoteFilePath = sys.argv[9]

def parseTitleHtml():
    title = readTextFromFile(titleFilePath)
    return "<center><h2>%s</h2></center>\n\n" % title

def parseAuthorHtml():
    authorList = readVectorFromFile(authorsFilePath)
    institutionsList = readVectorFromFile(institutionsFilePath)
    titleFootnote = readTextFromFile(titleFootnoteFilePath)

    institutionOrderDict = {}
    institutionNames = []
    for i in range(len(institutionsList)):
        institutionKey = institutionsList[i].split("=")[0]
        institutionName = institutionsList[i].split("=")[1]

        institutionOrderDict[institutionKey] = str(i+1)
        institutionNames.append(institutionName)

    authorHtmlItems = []
    for i in range(len(authorList)):
        author = authorList[i]
        authorName = author.split("=")[0]

        affiliations = []
        for affiliation in author.split("=")[1].split(","):
            if affiliation in institutionOrderDict.keys():
                affiliations.append(institutionOrderDict[affiliation])
            else:
                affiliations.append(affiliation)

        authorHtmlItems.append("%s<sup>%s</sup>" % (authorName, ",".join(affiliations)))

    authorHtml = "; ".join(authorHtmlItems) + "\n\n"

    authorHtml += "<ol>"
    for i in range(len(institutionNames)):
        authorHtml += "<li>%s" % institutionNames[i]
    authorHtml += "</ol>\n\n"

    if titleFootnote != "":
        authorHtml += titleFootnote + "\n\n"

    return authorHtml

def parseBibFile(bibFilePath):
    bibDict = {}

    for entry in readTextFromFile(bibFilePath).split("\n@"):
        if entry.split("\n")[0].find("{") == -1: #These if statements accommodate header lines in .bib files
            continue
        if entry.startswith("@"):
            entry = entry[1:]

        entryKey, entryType, entryDict = parseBibEntry(entry)
        bibDict[entryKey] = (entryType, entryDict)

    return bibDict

def parseBibEntry(entry):
    entryLines = entry.rstrip().split("\n")
    firstLine = entryLines.pop(0)
    entryType = firstLine.split("{")[0]
    entryKey = firstLine.split("{")[1].replace(",", "")

    entryDict = {}

    for line in entryLines:
        if line.find(" = ") == -1:
            continue

        key = line.split(" = ")[0]
        value = line.split(" = ")[1]

        if value.endswith(","):
            value = value[:-1]
        value = value.lstrip().rstrip()[1:-1] # Remove leading and trailing squiggly bracket

        entryDict[key] = value

    return entryKey, entryType, entryDict

def formatCitationKeysNames(keys, bibDict):
    formattedItems = []

    for key in keys:
        entryType, entryDict = bibDict[key]
        author = getCitationDictValue(entryDict, "author").split(" and ")[0].split(",")[0]
        year = getCitationDictValue(entryDict, "year")
        formattedItems.append("%s %s" % (author.replace("{", "").replace("}", ""), year))

    return " (%s)" % "; ".join(formattedItems)

def formatCitationKeysNumbers(keys, citationOrderDict, suppStart, suppEnd):
    if len(keys) == 1:
        return "%s%i%s" % (suppStart, citationOrderDict[keys[0]], suppEnd)

    numbers = sorted([citationOrderDict[key] for key in keys])
    numberGroups = []
    numberGroup = [numbers[0]]
    for i in range(1, len(numbers)):
        if numbers[i] == (numbers[i-1] + 1):
            numberGroup.append(numbers[i])
        else:
            numberGroups.append(numberGroup)
            numberGroup = [numbers[i]]
    numberGroups.append(numberGroup)

    formatted = []
    for numberGroup in numberGroups:
        if len(numberGroup) == 1:
            formatted.append(str(numberGroup[0]))
        else:
            modNumberGroup = [str(min(numberGroup)), str(max(numberGroup))]
            formatted.append("-".join(modNumberGroup))

    return "%s%s%s" % (suppStart, ",".join(formatted), suppEnd)

def getCitationDictValue(bibDict, key):
    if key in bibDict:
        return bibDict[key]
    else:
        return "???"

def buildReferenceHtml(bibDict, citationKeys):
    medlineEntryParts = getMedlineEntryParts(medlineFilePath)
    properNouns = []
    if os.path.exists(properNounsFilePath):
        properNouns = [x.rstrip() for x in file(properNounsFilePath)]

    output = "<h3>References</h3>\n\n"

    for i in range(len(citationKeys)):
        key = citationKeys[i]
        entryType, entryDict = bibDict[key]

        reformat = True
        if entryType.find("_noreformat") > -1:
            entryType = entryType.replace("_noreformat", "")
            reformat = False

        if reformat:
            if entryDict.has_key("journal"):
                entryDict["journal"] = getJournalAbbreviation(entryDict["journal"], medlineEntryParts).rstrip()
            if entryDict.has_key("title"):
                entryDict["title"] = formatTitle(entryDict["title"], bibStyle, properNouns)
            if entryDict.has_key("author"):
                entryDict["author"] = formatAuthor(entryDict["author"], bibStyle)
            if entryDict.has_key("editor"):
                entryDict["editor"] = formatEditor(entryDict["editor"], bibStyle)

        templateText = bibStyle.getTemplate(entryType)
        for key in entryDict:
            templateText = templateText.replace("#" + key + "#", entryDict[key])

        for match in re.finditer("<[\#\w\d\s\.\-,;:\(\)]+>", templateText):
            token = match.group(0)
            if token.find("#") > -1:
                templateText = templateText.replace(token, "")

        templateText = templateText.replace("<", "").replace(">", "")
        templateText = templateText.replace("?.", "?") #This is a workaround for now. If a reference ends with a question mark, we don't want to also have a period after it. However, this should be generalized to other sentence-ending punctuation marks.

        output += "%i. %s\n\n" % (i + 1, templateText)

    return output

def convertTexToHtml(html):
    html = re.sub(r'\\begin{abstract}', r'\\section*{Abstract}', html)
    html = re.sub(r'\\end{abstract}', r'', html)
    html = re.sub(r'\\section\*{(.*?)}', r'<h3>\1</h3>', html)
    html = re.sub(r'\\subsection\*{(.*?)}', r'<h4>\1</h4>', html)
    html = re.sub(r'\\textit{(.*?)}', r'<i>\1</i>', html)
    html = re.sub(r'\\textbf{(.*?)}', r'<b>\1</b>', html)
    html = re.sub(r'\\underline{(.*?)}', r'<u>\1</u>', html)
    html = re.sub(r'\\hl{(.*?)}', r'<span style="background-color:yellow;">\1</span>', html)
    html = re.sub(r'\\url{(.*?)}', r"<a href='\1'>\1</a>", html)
    html = re.sub(r'\$\\(.*?)\$', r'&\1;', html)
    html = re.sub(r'\\noindent{(.*?)}', r'\1', html)
    html = re.sub(r'\\noindent ', r'', html)
    html = re.sub(r'\\vspace{(.*?)}', r'', html)
    html = re.sub(r'\\label{(.*?)}', r'', html)

    specialCharacterList = []
    for letter in ("a", "e", "i", "\\i", "o", "u", "z", "A", "E", "I", "O", "U"):
        specialCharacterList.append(["\\`%s" % letter, "&%sgrave;" % letter])
        specialCharacterList.append(["\\'%s" % letter, "&%sacute;" % letter])
        specialCharacterList.append(["\\^%s" % letter, "&%scirc;" % letter])
        specialCharacterList.append(["\\~%s" % letter, "&%stilde;" % letter])
        specialCharacterList.append(["\\\"%s" % letter, "&%suml;" % letter])
        specialCharacterList.append(["\\%s " % letter, "&%sslash;" % letter])
        #specialCharacterList.append(["\\%s " % letter, "&%sring;" % letter])

    specialCharacterList.append(["\\%", "%"])
    specialCharacterList.append(["\\&", "&amp;"])
    specialCharacterList.append(["\\#", "#"])
    specialCharacterList.append(["``", "&quot;"])
    specialCharacterList.append(["''", "&quot;"])
    specialCharacterList.append(["---", "&mdash;"])
    specialCharacterList.append(["--", "&ndash;"])
    specialCharacterList.append(["\\clearpage", ""])
    specialCharacterList.append(["\\begin{center}", "<center>"])
    specialCharacterList.append(["\\end{center}", "</center>"])
    specialCharacterList.append(["\\ldots", "..."])

    for char in specialCharacterList:
        html = html.replace(char[0], char[1])

    mathCharacterList = []
    mathCharacterList.append(["^\\circ", "&deg;"])
    for symbol in ["alpha", "beta", "gamma", "delta", "epsilon", "varepsilon", "zeta", "eta", "theta", "vartheta", "gamma", "kappa", "lambda", "mu", "nu", "xi", "o", "pi", "varpi", "rho", "varrho", "sigma", "varsigma", "tau", "upsilon", "phi", "varphi", "chi", "psi", "omega", "iota"]:
        mathCharacterList.append(["\\%s" % symbol, "&%s;" % symbol])

    for char in mathCharacterList:
        html = html.replace("$" + char[0] + "$", char[1])

    lines = [line for line in html.split("\n") if line.strip() != "" and not "\\renewcommand" in line and not "\\setcounter" in line]
    html = ""
    for line in lines:
        if line.startswith("<") and not line.startswith("<b>"):
            html += line + "\n\n"
        else:
            html += "<p>" + line + "</p>\n\n"

    return html

def replaceCites(text, suppStart, suppEnd, useCitationNames):
    citationCoordinates = []
    citationCount = 1
    citationOrderDict = {}
    citationsFormatted = []
    uniqueCitationKeys = []

    for match in re.finditer(r"\\cite{.*?}", text):
        matchCitationKeys = match.group(0).replace("\cite{", "").replace("}", "").split(",")
        citationCoordinates.append([match.start(), match.end()])

        for citationKey in matchCitationKeys:
            if citationKey not in citationOrderDict.keys():
                citationOrderDict[citationKey] = citationCount
                citationCount += 1

            if citationKey not in uniqueCitationKeys:
                uniqueCitationKeys.append(citationKey)

        if useCitationNames:
            formatted = formatCitationKeysNames(matchCitationKeys, bibDict)
        else:
            formatted = formatCitationKeysNumbers(matchCitationKeys, citationOrderDict, suppStart, suppEnd)
        citationsFormatted.insert(0, formatted)

    for coordinates in reversed(citationCoordinates):
        text = text[:coordinates[0]] + citationsFormatted.pop(0) + text[coordinates[1]:]

    return text, uniqueCitationKeys

if os.path.exists(authorsFilePath):
    html = "<html><body>\n\n" + parseTitleHtml() + parseAuthorHtml() + readTextFromFile(inFilePath) + "\n\n</body></html>"
else:
    print "The author file could not be found, so it is assumed this is Supplementary Material."
    html = "<html><body>\n\n" + parseTitleHtml() + "<center><h3>Supplementary Material</h3></center>\n\n" + readTextFromFile(inFilePath) + "\n\n</body></html>"

bibDict = parseBibFile(bibFilePath)

#html, uniqueCitationKeys = replaceCites(html, "<sup>", "</sup>", True)
html, uniqueCitationKeys = replaceCites(html, "<sup>", "</sup>", False)
html = html.replace("\\bibliography{library}", buildReferenceHtml(bibDict, uniqueCitationKeys))
html = convertTexToHtml(html)

#use.names.in.citation.references

writeScalarToFile(html, inFilePath)
