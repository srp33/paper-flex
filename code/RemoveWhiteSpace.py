import os, sys, glob
from utilities import *

filePath = sys.argv[1]

lines = [line for line in file(filePath) if line.strip() != ""]

writeVectorToFile(lines, filePath)
