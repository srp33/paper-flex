import os, sys, glob

inFilePath = sys.argv[1]
match = sys.argv[2]

lines = []

for line in file(inFilePath):
    if match in line:
        break

    lines.append(line)

outFile = open(inFilePath, 'w')
for line in lines:
    outFile.write(line)
outFile.close()
