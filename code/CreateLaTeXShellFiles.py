import os, sys, glob, shutil
from utilities import *
from LatexUtilities import *

preambleFilePath = sys.argv[1]
titleFilePath = sys.argv[2]
authorFilePath = sys.argv[3]
manuscriptFilePath = sys.argv[4]
preManuscriptFilePath = sys.argv[5]

def hasCitations(textLines):
    return len([x for x in textLines if "\cite{" in x.replace("*", "").replace("[", "{").replace("p{", "{").replace("t{", "{")]) > 0

def addBibliography(textLines, label, isFullSupplementary=False):
    bibLine = "\\clearpage\n\n\\bibliography{library}\n\n\\label{%s}" % label

    if hasCitations(textLines):
        for i in range(len(textLines)):
            if textLines[i] == "[References]":
                textLines[i] = bibLine

        if isFullSupplementary:
            textLines.append(bibLine)

    return textLines

def addTableOfContents(numFigures, numTables, numMethods, numData, hasCites):
    result = ["\\clearpage", "\\section*{Table of Contents}", "\\begin{specifications}"]

    if numTables > 0:
        result.append("[TableOfContentsTables]")
    if numFigures > 0:
        result.append("[TableOfContentsFigures]")
    if numData > 0:
        result.append(getDotLeaderItem("Supplementary Data Descriptions", "SupplementaryData"))
    if numMethods > 0:
        result.append(getDotLeaderItem("Supplementary Methods", "SupplementaryMethods"))
    if hasCites:
        result.append(getDotLeaderItem("References", "SupplementaryReferences"))

    return result + ["\\end{specifications}"]

def getSupplementaryOther(manuscriptLines, otherDescription):
    supplementaryOtherLineIndices = []

    inSupplementaryOtherSection = False
    for i in range(len(manuscriptLines)):
        if manuscriptLines[i].strip().startswith("\\section*{Supplementary %s" % otherDescription) and not manuscriptLines[i].strip().startswith("\\section*{Supplementary Information}"):
            inSupplementaryOtherSection = True
        else:
            if manuscriptLines[i].startswith("\\section*{"):
                inSupplementaryOtherSection = False

        if inSupplementaryOtherSection:
            supplementaryOtherLineIndices.append(i)

    supplementaryOtherLines = []
    if len(supplementaryOtherLineIndices) > 0:
        supplementaryOtherLines = []
        for i in supplementaryOtherLineIndices:
            line = manuscriptLines[i]
            if manuscriptLines[i].strip().startswith("\\section*{Supplementary %s" % otherDescription) and not manuscriptLines[i].strip().startswith("\\section*{Supplementary Information}"):
                supplementaryOtherLines.extend(clearLines)
            supplementaryOtherLines.append(line + "\n\n")

            if len(supplementaryOtherLines) == 1:
                supplementaryOtherLines.append("\\label{Supplementary%s}\n\n" % otherDescription)

        manuscriptLines = [manuscriptLines[i] for i in range(len(manuscriptLines)) if i not in supplementaryOtherLineIndices]

    return manuscriptLines, supplementaryOtherLines

preambleLines = readVectorFromFile(preambleFilePath)
titleLine = readVectorFromFile(titleFilePath)[0]
authorLinesRaw = readVectorFromFile(authorFilePath)
manuscriptLines = readVectorFromFile(manuscriptFilePath)

beginLines = ["\\begin{document}", "\\pagenumbering{gobble}", "\\maketitle", "\\doublespacing"]
clearLines = ["\\clearpage"]
endLines = ["\\end{document}"]
pageNumberLines = ["\\pagenumbering{arabic}"]
#supplementaryPrefixLines = ["\\renewcommand{\\figurename}{Supplementary Figure}", "\\renewcommand{\\tablename}{Supplementary Table}", "\\setcounter{figure}{0}", "\\setcounter{table}{0}"]
#supplementaryPrefixLines = ["\\renewcommand{\\thefigure}{S\\arabic\\c@figure}", "\\renewcommand{\\thetable}{S\\arabic\\c@table}", "\\setcounter{figure}{0}", "\\setcounter{table}{0}"]
supplementaryPrefixLines = ["\\renewcommand{\\thepage}{S\\arabic{page}}", "\\renewcommand{\\thesection}{S\\arabic{section}}", "\\renewcommand{\\thefigure}{S\\arabic{figure}}", "\\renewcommand{\\thetable}{S\\arabic{table}}", "\\setcounter{figure}{0}", "\\setcounter{table}{0}", ""]
supplementaryHeaderLines = ["\\newenvironment{specifications}{%", "\\let\\olditem\\item%", "\\renewcommand\\item[2][]{\\olditem##1\\dotfill##2}%", "\\begin{description}}{\\end{description}%", "}", "", "\\usepackage{hyperref}", ""]

manuscriptTitleLines = ["\\title{\\textbf{%s}}" % titleLine]
supplementarySubTitleLines = ["\\begin{center}", "\\Large", "\\textbf{\\textit{Supplementary Material}}", "\\end{center}"] + clearLines

authorLines = []
for line in authorLinesRaw:
    if line.strip().startswith("\\author"):
        authorLines.append("\\begin{center}")

    authorLines.append(line)

    if line.strip().startswith("\\author"):
        authorLines.append("\\end{center}")

supplementaryFigureLines = [x + "\n" for x in manuscriptLines if x.strip().startswith("[SupplementaryFigure")]
supplementaryTableLines = [x + "\n" for x in manuscriptLines if x.strip().startswith("[SupplementaryTable")]
supplementaryDataLines = [x + "\n" for x in manuscriptLines if x.strip().startswith("[SupplementaryData")]
manuscriptLines = [x for x in manuscriptLines if not x.strip().startswith("[SupplementaryFigure") and not x.strip().startswith("[SupplementaryTable") and not x.strip().startswith("[SupplementaryData")]

manuscriptLines, supplementaryMethodsLines = getSupplementaryOther(manuscriptLines, "Methods")

if len(supplementaryFigureLines) > 0:
    supplementaryFigureLines = ["\\section*{Supplementary Figures}"] + supplementaryFigureLines + clearLines
if len(supplementaryTableLines) > 0:
    supplementaryTableLines = ["\\section*{Supplementary Tables}"] + supplementaryTableLines + clearLines
if len(supplementaryDataLines) > 0:
    supplementaryDataLines = ["\\section*{Supplementary Data Descriptions}\n\\label{SupplementaryData}"] + supplementaryDataLines + clearLines

numSuppItems = len(supplementaryFigureLines + supplementaryTableLines + supplementaryDataLines + supplementaryMethodsLines)

if numSuppItems:
    supplementaryLines = supplementaryTableLines + clearLines + supplementaryFigureLines + clearLines + supplementaryMethodsLines + clearLines + supplementaryDataLines

    writeVectorToFile(addBibliography(manuscriptLines, "References"), "ManuscriptInclude.tex")
    writeVectorToFile(addBibliography(supplementaryLines, "SupplementaryReferences"), "SupplementaryInclude.tex")
    writeVectorToFile(preambleLines + manuscriptTitleLines + beginLines + authorLines + clearLines + pageNumberLines, "ManuscriptInput.tex")
    writeVectorToFile(["\\includeonly{ManuscriptInclude,SupplementaryInclude}", "\\input{ManuscriptInput}", "\\include{ManuscriptInclude}", "\\include{SupplementaryInclude}"] + endLines, preManuscriptFilePath)
    writeVectorToFile(["\\includeonly{ManuscriptInclude}", "\\input{ManuscriptInput}", "\\include{ManuscriptInclude}", "\\include{SupplementaryInclude}"] + endLines, manuscriptFilePath)

    writeVectorToFile(preambleLines + supplementaryHeaderLines + manuscriptTitleLines + beginLines + supplementarySubTitleLines + pageNumberLines + supplementaryPrefixLines + addTableOfContents(len(supplementaryTableLines), len(supplementaryFigureLines), len(supplementaryMethodsLines), len(supplementaryDataLines), hasCitations(supplementaryLines)) + clearLines + addBibliography(supplementaryLines, "SupplementaryReferences", True) + endLines, "Supplementary.tex")
else:
    print "No supplementary"
    writeVectorToFile(manuscriptLines + getBibliographyLines(manuscriptLines), "ManuscriptInclude.tex")
    writeVectorToFile(preambleLines + manuscriptTitleLines + beginLines + authorLines + clearLines + pageNumberLines, "ManuscriptInput.tex")
    writeVectorToFile(["\\includeonly{ManuscriptInclude}", "\\input{ManuscriptInput}", "\\include{ManuscriptInclude}"] + endLines, preManuscriptFilePath)
    writeVectorToFile(["\\includeonly{ManuscriptInclude}", "\\input{ManuscriptInput}", "\\include{ManuscriptInclude}"] + endLines, manuscriptFilePath)

#    writeVectorToFile(preambleLines + manuscriptTitleLines + beginLines + authorLines + clearLines + manuscriptLines + getBibliographyLines(manuscriptLines) + endLines, manuscriptFilePath)
#    if os.path.exists("Supplementary.tex"):
#        os.remove("Supplementary.tex")
