import os, sys, glob
from utilities import *

inFilePath = sys.argv[1]
sectionsToKeep = sys.argv[2].split(",")

modSectionsToKeep = set()
for section in sectionsToKeep:
    modSectionsToKeep.add("\section*{%s}" % section)
    modSectionsToKeep.add("<h3>%s</h3>" % section)

inLines = readVectorFromFile(inFilePath)
outLines = []

inSectionToKeep = False
for line in inLines:
    line = line.rstrip()

    if "html>" in line:
        outLines.append(line)

    if line in modSectionsToKeep:
        inSectionToKeep = True
    else:
        if line.startswith("\section*{") or line.startswith("<h3>"):
            inSectionToKeep = False

    if inSectionToKeep and "html>" not in line:
        outLines.append(line)

writeVectorToFile(outLines, inFilePath)
