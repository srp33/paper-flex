import os, sys, glob, re, shutil
from utilities import *
from LatexUtilities import *
from HtmlUtilities import *

mainTexFilePath = sys.argv[1]
suppIncludeTexFilePath = sys.argv[2]
suppTexFilePath = sys.argv[3]
mainHtmlFilePath = sys.argv[4]
suppIncludeHtmlFilePath = sys.argv[5]
outputDirPath = sys.argv[6]

def process(tex, html, tagPrefix, labelCountPrefix):
    dataCount = 0
    labelOrderDict = {}

    if tex == None:
        return tex, html, labelOrderDict

    for dataMatch in re.finditer("\[%s:.*\]" % tagPrefix, tex):
        dataCount += 1

        token = dataMatch.group(0)
        tokenItems = token.replace("[%s:" % tagPrefix, "").replace("]", "").split("|")
        filePath, fileExtension = os.path.splitext(tokenItems[0])
        filePath = filePath + fileExtension
        description = tokenItems[1]

        if not os.path.exists(filePath):
            print "No data file exists at %s" % filePath
            sys.exit(1)

#        labelOrderDict[label] = dataCount

        expanded = "\\noindent \\textbf{Supplementary Data File %i}: %s\n\n\\vspace{5 mm}\n\n" % (dataCount, description)

        destFilePath = "%s/Supplementary_Data_File_%i%s" % (outputDirPath, dataCount, fileExtension)
        print "Copying data file from %s to %s" % (filePath, destFilePath)
        shutil.copy(filePath, destFilePath)

        descriptionFilePath = "%s/Supplementary_Data_File_%i__Description.txt" % (outputDirPath, dataCount)
        print "Writing data description file to %s" % descriptionFilePath
        writeScalarToFile(description, descriptionFilePath)

        tex = tex.replace(token, expanded)

        if html != None:
            dataHtml = "<p><b>Supplementary Data File %i</b>: %s</p>" % (dataCount, description)
            html = html.replace(token, dataHtml)

    return tex, html, labelOrderDict

mainTex = readTextFromFile(mainTexFilePath)
mainHtml = readTextFromFile(mainHtmlFilePath)

suppIncludeTex = None
suppTex = None
if os.path.exists(suppTexFilePath):
    suppIncludeTex = readTextFromFile(suppIncludeTexFilePath)
    suppTex = readTextFromFile(suppTexFilePath)

suppIncludeHtml = None
if os.path.exists(suppIncludeHtmlFilePath):
    suppIncludeHtml = readTextFromFile(suppIncludeHtmlFilePath)

mainTex, mainHtml, mainLabelOrderDict = process(mainTex, mainHtml, "SupplementaryData", "")
suppIncludeTex, suppIncludeHtml, suppLabelOrderDict = process(suppIncludeTex, suppIncludeHtml, "SupplementaryData", "S")
suppTex, ignore1, ignore2 = process(suppTex, None, "SupplementaryData", "S")

#mainTex = convertRefsToNumbers(mainTex, mainLabelOrderDict, suppLabelOrderDict)
#mainHtml = convertRefsToNumbers(mainHtml, mainLabelOrderDict, suppLabelOrderDict)

writeScalarToFile(mainTex, mainTexFilePath)
writeScalarToFile(mainHtml, mainHtmlFilePath)

if suppIncludeTex != None:
#    suppIncludeTex = convertRefsToNumbers(suppIncludeTex, mainLabelOrderDict, suppLabelOrderDict)
#    suppIncludeHtml = convertRefsToNumbers(suppIncludeHtml, mainLabelOrderDict, suppLabelOrderDict)
#    suppTex = convertRefsToNumbers(suppTex, mainLabelOrderDict, suppLabelOrderDict)

    writeScalarToFile(suppIncludeTex, suppIncludeTexFilePath)
    writeScalarToFile(suppIncludeHtml, suppIncludeHtmlFilePath)
    writeScalarToFile(suppTex, suppTexFilePath)
