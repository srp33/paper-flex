import os, sys, glob, re
import utilities

inFilePath = sys.argv[1]

tex = utilities.readTextFromFile(inFilePath)

refsRaw = []
for match in re.finditer("\\\\ref{.*?}", tex):
    refRaw = match.group(0).replace("\\ref{", "").replace("}", "")
    for x in refRaw.split(","):
        refsRaw.append(x)

refs = []
for ref in refsRaw:
    if ref not in refs and ref.strip() != "":
        refs.append(ref)

preFigTags = []
preFigDict = {}
for preFigTag in [x.group(0) for x in re.finditer("\[.*Figure:.*?\]", tex)]:
    preFigTags.append(preFigTag)
    preFigName = preFigTag.split(":")[1].split("|")[0]
    preFigDict[preFigName] = preFigTag

preTableTags = []
preTableDict = {}
for preTableTag in [x.group(0) for x in re.finditer("\[.*Table:.*?\]", tex)]:
    preTableTags.append(preTableTag)
    preTableName = preTableTag.split(":")[1].split("|")[0]
    preTableDict[preTableName] = preTableTag

nonReferencedItems = (set(preFigDict.keys()) | set(preTableDict.keys())) - set(refs)
if len(nonReferencedItems) > 0:
    print "The following items have not been referenced:"
    for x in sorted(nonReferencedItems):
        print "  " + x
    exit(1)

orphanReferences = set(refs) - (set(preFigDict.keys()) | set(preTableDict.keys()))
if len(orphanReferences) > 0:
    print "The following references are orphans:"
    for x in sorted(orphanReferences):
        print "  " + x
    exit(1)

postFigTags = []
for ref in refs:
    if ref in preFigDict:
        postFigTags.append(preFigDict[ref])

postTableTags = []
for ref in refs:
    if ref in preTableDict:
        postTableTags.append(preTableDict[ref])

for i in range(len(preFigTags)):
    tex = tex.replace(preFigTags[i], "[Figure%i]" % i)
for i in range(len(postFigTags)):
    tex = tex.replace("[Figure%i]" % i, postFigTags[i])

for i in range(len(preTableTags)):
    tex = tex.replace(preTableTags[i], "[Table%i]" % i)
for i in range(len(postTableTags)):
    tex = tex.replace("[Table%i]" % i, postTableTags[i])

utilities.writeScalarToFile(tex, inFilePath)
