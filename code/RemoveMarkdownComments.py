import sys, re
from utilities import *

filePath = sys.argv[1]

fileText = readTextFromFile(filePath)
fileText = re.sub("<!\-\-\-.*\-\-?\->", "", fileText, flags=re.DOTALL)
writeTextToFile(fileText, filePath)
