import os, sys, glob, re
from utilities import *
from LatexUtilities import *
from operator import itemgetter, attrgetter

inFilePath = sys.argv[1]
acronymsFilePath = sys.argv[2]

acronyms = []
for line in file(acronymsFilePath):
    acronyms.append([line.rstrip().split("=")[0], line.rstrip().split("=")[1]])

text = readTextFromFile(inFilePath)

for acronym in acronyms:
    for match in re.finditer(acronym[0], text):
        if isInCiteOrRefTag(text[:match.start()]):
            continue

        openParenChar = "("
        closeParenChar = ")"

        if isInParenthesis(text, match.start()):
            openParenChar = "["
            closeParenChar = "]"

        if text[match.end()] == "s":
            expandedAcronym = "%ss %s%ss%s" % (acronym[1], openParenChar, acronym[0], closeParenChar)
            text = text[:match.start()] + expandedAcronym + text[(match.start() + len(acronym[0]) + 1):]
        else:
            expandedAcronym = "%s %s%s%s" % (acronym[1], openParenChar, acronym[0], closeParenChar)
            text = text[:match.start()] + expandedAcronym + text[(match.start() + len(acronym[0])):]

        break

writeScalarToFile(text, inFilePath)
