* Parse and format references in Tex file using Python code
* Refine CreateLaTeXShellFiles so that it no longer requires include files and then simplify Expand*Tags.py
* Make sure that when you simplify the Main/Supplementary flow that reference #'s continue from where they left off in main.
* Parse and format author info for Tex
* Exclude comments that are in the middle of a line (but not \%)
* Better error messages when parsing references and a piece is missing. It needs to fail on that part and not leave #'s.
* Add option to list acronyms at the top of the manuscript.
* Only expand (and list) acronyms if they are cited more than once.
* When you specify that only certain sections should be included, do this for PDF as well as doc
* When combining \ref values, support subfigures ("Figure 5A and Table 2")
* Enable automatic diff versioning and highlighting
* Make it so it will search for any file with a .bib extension and concatenate them.
* Put ~ before \cite{ if the citations are not using numbers.
* Support "boxes" - little boxes of text that are in a background color and highlight a particular issue.
* Make it so you can reference supplementary data files by number.
* References not being added to Supplementary Word document.
* Don't place citation before period if the sentence ends in et al.
* Change HTML quotes to be slanty ones rather than up and down.
* Get rid of places where two spaces are accidentally added?
* Make it so figure, table, data, and equation references do not require you to put "Figure" in the manuscript text. Based on a user configuration, have it output "Figure 1" or "Figure S1" or "Supplementary Figure 1" depending on the journal's requirements.
* Manuscript in Word should be double spaced.
