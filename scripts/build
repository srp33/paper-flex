#!/bin/bash

set -o errexit

inputFileName="$1"
#inputFileName="Manuscript.md"

if [ "$inputFileName" == "" ]
then
  inputFileName=Manuscript.md
fi

## Declare variables
home=/home/vagrant
codeDir=$home/paper-flex/code
scriptsDir=$home/paper-flex/scripts
miscDir=$home/paper-flex/misc
inputFileDescription=${inputFileName/\.md/}
inputFile=$home/Input/$inputFileName
inputFiguresDir=$home/Input/Figures
inputTablesDir=$home/Input/Tables
inputSupplementaryDataDir=$home/Input/Supplementary_Data
tempDir=/tmp/$inputFileDescription
outDir=$home/Output/$inputFileDescription
outMarkdownFile=$outDir/Manuscript.md
outSuppMarkdownFile=${outDir}/Supplementary.md
outHtmlFile=$outDir/Manuscript.html
outSuppHtmlFile=${outDir}/Supplementary.html
outDocFile=$outDir/Manuscript.docx
outSuppDocFile=${outDir}/Supplementary.docx
outPdfFile=$outDir/Manuscript.pdf
outSuppPdfFile=${outDir}/Supplementary.pdf
outFiguresDir=$outDir/Figures
outTablesDir=$outDir/Tables
outEquationsDir=$outDir/Equations
outSupplementaryDataDir=$outDir/Supplementary_Data

## Copy relevant files to temp directory
rm -rf $tempDir
mkdir -p $tempDir
cp -rv $home/Input/* $tempDir/
#cat $tempDir/*.bib > $tempDir/library.bib

cd $tempDir

## Clean up the input text
python $codeDir/RemoveMarkdownComments.py $inputFileName
python $codeDir/RemoveWhiteSpace.py $inputFileName

## Check to make sure all figures, tables, etc. are referenced in the text.
## Check to make sure all references are pointing to an actual figure, table, etc.
## Changing the names of tags based on their ordering.
#### Should we store the figure information in a separate file for each?
#### Should we integrate this logic with the Expand*.py and/or CreateLaTexShellFiles.py scripts?
#### Look up pandoc approaches for reference syntax and for bibliographies, etc.
python $codeDir/CheckAndReorderFiguresAndTables.py $inputFileName
cat $inputFileName
exit

##if [ -f $home/$home/Input/Acronyms.txt ]
##then
##  python $codeDir/CheckAcronyms.py $inputFileName $home/Input/Acronyms.txt
##fi

#### Refine CreateLatexShellFiles.py so AuthorInfo logic is pulled from .txt file rather than .tex
##python $codeDir/CreateLaTeXShellFiles.py $miscDir/Preamble.tex Title.txt Authors.txt Manuscript.tex PreManuscript.tex

cp ManuscriptInclude.tex Manuscript.html

if [ -f Supplementary.tex ]
then
  cp SupplementaryInclude.tex Supplementary.html
fi


python $currentDir/code/ExpandFigureTags.py ManuscriptInclude.tex SupplementaryInclude.tex Supplementary.tex Manuscript.html Supplementary.html $outputDir
python $currentDir/code/ExpandTableTags.py ManuscriptInclude.tex SupplementaryInclude.tex Supplementary.tex Manuscript.html Supplementary.html $outputDir
python $currentDir/code/ExpandDataTags.py ManuscriptInclude.tex SupplementaryInclude.tex Supplementary.tex Manuscript.html Supplementary.html $outputDir

#python $currentDir/code/ExpandEquationTags.py ManuscriptInclude.tex Manuscript.html
#cd Equations
#for f in *.tex
#do
#  if [ -f $f ]
#  then
#    ./tex2im -r 300x300 $f
#    mv -v ${f/\.tex/.png} ../
#  fi
#done
#cd ..

python $currentDir/code/FormatTexHtml.py Manuscript.html library.bib ama.style $miscDir/J_Medline.txt None Title.txt Authors.txt Institutions.txt TitleFootnote.txt

if [ -f Supplementary.tex ]
then
  python $currentDir/code/FormatTexHtml.py Supplementary.html library.bib ama.style $miscDir/J_Medline.txt None Title.txt None None None

  ##python $currentDir/code/ExpandEquationTags.py Supplementary.tex Supplementary.tex False
  ##python $currentDir/code/ExpandEquationTags.py SupplementaryInclude.tex SupplementaryInclude.tex False
fi

pdflatex PreManuscript
pdflatex Manuscript

if [ -f Supplementary.tex ]
then
  cp SupplementaryInclude.aux SupplementaryInclude2.aux
  python RemoveLinesAfterMatch.py SupplementaryInclude.aux "\\bibdata"
fi

bibtex Manuscript
python ProcessReferences.py Manuscript.bbl bibitem library.bib ama.style None
pdflatex Manuscript
pdflatex Manuscript

if [ -f Manuscript.pdf ]
then
  cp Manuscript.pdf $outputDir/$outPdfFile
  open $outputDir/$outPdfFile
fi

if [ -f Supplementary.tex ]
then
  cp SupplementaryInclude2.aux SupplementaryInclude.aux
  pdflatex Supplementary

  fileHasBib=`python $currentDir/code/FileContainsText.py SupplementaryInclude.tex \cite{`
  if [ "$fileHasBib" == "True" ]
  then
    bibtex Supplementary
    python ProcessReferences.py Supplementary.bbl bibitem library.bib ama.style None
  fi

  pdflatex Supplementary
  pdflatex Supplementary

  if [ -f Supplementary.pdf ]
  then
    cp Supplementary.pdf $outputDir/$outSuppPdfFile
    open $outputDir/$outSuppPdfFile
    cp Supplementary.html $outputDir/Supplementary.doc
  fi
fi

if [ -f Manuscript.html ]
then
  if [ "$sectionsToOutput" != "" ]
  then
    python $currentDir/code/KeepOnlyCertainSections.py Manuscript.html $sectionsToOutput
  fi

  cp -v Manuscript.html $outputDir/Manuscript.doc
#  open $outputDir/Manuscript.doc
#  pandoc -s Manuscript.html -o Manuscript.docx
#  cp -f Manuscript.docx $outputDir/Manuscript.docx
#  open $outputDir/Manuscript.docx
fi

#./texcount.pl -sub=section ManuscriptInclude.tex | tee $outputDir/Manuscript_WordStats.txt
#./texcount.pl -sub=section SupplementaryInclude.tex | tee $outputDir/Supplementary_WordStats.txt

function zipOutputFiles {
  rm -f $outputDir/${1}s.zip

  zipFiles=$(ls $outputDir/${1}_* 2> /dev/null | wc -l)
  if [ "$zipFiles" != "0" ]
  then
    thisDir=$(pwd)
    cd $outputDir
    zip ${1}s.zip ${1}_*
    cd $thisDir
  fi
}

zipOutputFiles Table
zipOutputFiles Figure
zipOutputFiles Supplementary_Data_File

## Remove temp files
